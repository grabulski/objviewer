#include "model.h"

Model::Model(const char *modelName, const char *textureName)
{
    std::ifstream in;
    in.open(modelName, std::ifstream::in);
    if (in.fail()) {
       std::cerr << "Failed to open " << modelName << std::endl;
       return;
    }

    std::string line;
    while (!in.eof()) {
        std::getline(in, line);
        std::istringstream iss(line.c_str());
        char trash;
        if (!line.compare(0, 2, "v ")) {
            iss >> trash;
            arma::vec4 v;
            for (int i=0;i<3;i++) iss >> v[i];
            v[3] = 1;
            verts.push_back(v);
        } else if (!line.compare(0, 3, "vt ")) {
            iss >> trash >> trash;
            arma::vec2 _uv;
            for (int i=0;i<2;i++) iss >> _uv[i];
            uv.push_back(_uv);
        } else if (!line.compare(0, 2, "f ")) {
            int f, t, n;
            int cnt=0;
            iss >> trash;
            while (iss >> f >> trash >> t >> trash >> n) {
                face_vrt.push_back(--f);
                face_tex.push_back(--t);
                ++cnt;
            }
            if (3!=cnt) {
                std::cerr << "Error: the obj file is supposed to be triangulated" << std::endl;
                in.close();
                return;
            }
        }
    }

    std::cout << "# v# " << nverts() << " f# "  << nfaces() << std::endl;

    texture = QImage(textureName);
    QTransform m;
    m.scale(1, -1);
    texture = texture.transformed(m);

    scaleVec[0] = scaleVec[1] = scaleVec[2] = 1;
}

int Model::nverts() const
{
    return (int)verts.size();
}

int Model::nfaces() const
{
    return (int)face_vrt.size()/3;
}

arma::vec4 Model::point(int pi)
{
    return verts[pi];
}

std::vector<arma::vec4> Model::face(int fi)
{
    double rxv = rotation[0];
    double ryv = rotation[1];
    double rzv = rotation[2];

    arma::mat rx = {
        {1, 0, 0, 0},
        {0, cos(rxv), -sin(rxv), 0},
        {0, sin(rxv), cos(rxv), 0},
        {0, 0, 0, 1}
    };

    arma::mat ry = {
        {cos(ryv), 0, sin(ryv), 0},
        {0, 1, 0, 0},
        {-sin(ryv), 0, cos(ryv), 0},
        {0, 0, 0, 1}
    };

    arma::mat rz = {
        {cos(rzv), -sin(rzv), 0, 0},
        {sin(rzv), cos(rzv), 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    arma::mat t = {
        {1, 0, 0, position[0]},
        {0, 1, 0, position[1]},
        {0, 0, 1, position[2]},
        {0, 0, 0, 1}
    };

    arma::mat s = {
        {scaleVec[0], 0, 0, 0},
        {0, scaleVec[1], 0, 0},
        {0, 0, scaleVec[2], 0},
        {0, 0, 0, 1}
    };

    std::vector<arma::vec4> f(3);

    for (int i = 0; i < 3; ++i) {
        arma::vec4 p = point(face_vrt[fi*3+i]);

        p = (t * (rx * (ry * (rz * (s *  p)))));

        f[i] = p;
    }

    return f;
}

std::vector<arma::vec2> Model::getUVs(int fi)
{
    std::vector<arma::vec2> uvs(3);
    for (int i = 0; i < 3; ++i){
        uvs[i] = uv[face_tex[fi*3+i]];
    }
    return uvs;
}

QColor Model::getTextureColor(float U, float V)
{
    int x = texture.width() * U;
    int y = texture.height() * V;
    if (x < 0 || x >= texture.width() || y < 0 || y >= texture.height())
        return QColor(255, 255, 255);
    return texture.pixelColor(x, y);
}

void Model::translate(arma::vec4 target)
{
//    arma::vec4 dt = target - position;
//    for (int i = 0; i < nverts(); ++i) {
//        verts[i] += dt;
//    }
    position = target;
}

void Model::scale(arma::vec3 target)
{
//    arma::vec3 dt = target ;//- scaleVec;
//    arma::mat s = {
//        {dt[0], 0, 0, 0},
//        {0, dt[1], 0, 0},
//        {0, 0, dt[2], 0},
//        {0, 0, 0, 1}
//    };

//    for (int i = 0; i < nverts(); ++i) {
//        verts[i] = s * (verts[i] - position);
//    }

    scaleVec = target;
}

void Model::rotateX(float alpha)
{
//    float dt = alpha - rotation[0];
//    arma::mat rx = {
//        {1, 0, 0, 0},
//        {0, cos(dt), -sin(dt), 0},
//        {0, sin(dt), cos(dt), 0},
//        {0, 0, 0, 1}
//    };
//    for (int i = 0; i < nverts(); ++i) {
//        auto p = verts[i];
//        verts[i] = rx * (p - position);
//        verts[i] += position;
//    }

    rotation[0] = alpha;
}

void Model::rotateY(float alpha)
{
//    float dt = alpha - rotation[1];
//    arma::mat ry = {
//        {cos(dt), 0, sin(dt), 0},
//        {0, 1, 0, 0},
//        {-sin(dt), 0, cos(dt), 0},
//        {0, 0, 0, 1}
//    };
//    for (int i = 0; i < nverts(); ++i) {
//        auto p = verts[i];
//        verts[i] = ry * (p - position);
//        verts[i] += position;
//    }

    rotation[1] = alpha;
}

void Model::rotateZ(float alpha)
{
//    float dt = alpha - rotation[2];
//    arma::mat ry = {
//        {cos(dt), -sin(dt), 0, 0},
//        {sin(dt), cos(dt), 0, 0},
//        {0, 0, 1, 0},
//        {0, 0, 0, 1}
//    };
//    for (int i = 0; i < nverts(); ++i) {
//        auto p = verts[i];
//        verts[i] = ry * (p - position);
//        verts[i] += position;
//    }

    rotation[2] = alpha;
}
