#ifndef MODEL_H
#define MODEL_H

#include <QImage>
#include <armadillo>

class Model
{
private:
    std::vector<arma::vec4> verts;
    std::vector<arma::vec3> faces;
    std::vector<int> face_vrt;
    std::vector<int> face_tex;
    std::vector<arma::vec2> uv;
    QImage texture;

public:
    arma::vec4 position;
    arma::vec3 rotation;
    arma::vec3 scaleVec;

    Model(const char *modelName, const char *textureName);

    int nverts() const;
    int nfaces() const;

    arma::vec4 point(int pi);
    std::vector<arma::vec4> face(int fi);
    std::vector<arma::vec2> getUVs(int fi);
    QColor getTextureColor(float U, float V);
    void translate(arma::vec4 target);
    void scale(arma::vec3 target);
    void rotateX(float alpha);
    void rotateY(float alpha);
    void rotateZ(float alpha);

};

#endif // MODEL_H
