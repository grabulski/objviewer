#ifndef RENDERER_H
#define RENDERER_H

#include "model.h"

#include <QWidget>

class Renderer : public QWidget
{
    Q_OBJECT
private:
    QImage image;
    Model model;
    arma::mat view;

    float camZ;

public:
    explicit Renderer(QWidget *parent = nullptr);

    void clearScreen();
    void render();
    void drawTriangle(arma::ivec4 t0, arma::ivec4 t1, arma::ivec4 t2, int fi);
    bool shouldDraw(arma::ivec4 t0, arma::ivec4 t1, arma::ivec4 t2);
    void insertPixel(int x, int y, int r, int g, int b, int a, QImage &img);
    void getBarycentric(arma::ivec4 &p, arma::ivec4 &a, arma::ivec4 &b, arma::ivec4 &c, float &u, float &v, float &w);

    arma::mat viewport(int x, int y, int w, int h);

protected:
    void paintEvent(QPaintEvent *);

public slots:
    void moveX(int x);
    void moveY(int y);
    void moveZ(int z);

    void rotX(int alpha);
    void rotY(int alpha);
    void rotZ(int alpha);

    void scaleX(int x);
    void scaleY(int y);
    void scaleZ(int z);
};

#endif // RENDERER_H
